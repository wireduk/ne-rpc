const fs = require('fs');
const Mustache = require('mustache');
const path = require('path');
const util = require('util');
const e = require('express');

const encoding = 'utf8';

const file = {
  readdir: util.promisify(fs.readdir),
  readFile: util.promisify(fs.readFile)
};

const events = [
  { 'name': 'The Inaugural NE-RPC', page: 'june-2020.mustache', data: 'june-2020.json', canonical: '2020-june' }
];

function slugify(s) {
  return s
    .toLowerCase()
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-") // collapse dashes
    .replace(/^-+/, "") // trim - from start of text
    .replace(/-+$/, "") // trim - from end of text
    .replace(/-/g, "-");
}

function buildCanoncialLinks(data) {
  data.sessions.forEach(s => {
    if (s.rooms) {
      s.rooms.forEach(r => {
        r.session.canonical = `${slugify(r.session.title)}.html`;
      });
    }
  });
}

async function readPartials() {
  const folder = "partials";

  const files = await file.readdir(folder);
  const partials = await Promise.all(
    files.map(f => file.readFile(path.join(folder, f), encoding)
      .then(data => ({
        title: f.split('.').slice(0, -1).join('.'),
        data
      }))
    )
  );
  return partials.reduce((agg, i) => {
    agg[i.title] = i.data;
    return agg;
  }, {});
}

function renderIndex(events, partials) {
  const data = {
    events
  };
  const template = fs.readFileSync('main.mustache', encoding);
  const output = Mustache.render(template, data, partials);
  fs.writeFileSync(path.join('public', 'index.html'), output, { encoding });
}

function renderEvent(event, partials) {
  const data = JSON.parse(fs.readFileSync(event.data, encoding));
  const dir = path.join('public', event.canonical);

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  buildCanoncialLinks(data);
  const eventTemplate = fs.readFileSync(event.page, encoding);
  const eventPage = Mustache.render(eventTemplate, data, partials);
  const fileName = path.join('public', event.canonical, 'index.html');
  fs.writeFileSync(fileName, eventPage, { encoding });
  renderSessions(data, event.canonical, partials);
}

function renderSessions(data, dir, partials) {
  const sessionTemplate = fs.readFileSync('session.mustache', encoding);
  data.sessions.forEach(s => {
    if (s.rooms) {
      s.rooms.forEach((r) => {
        const data = {
          session: r.session,
          slotStart: s.slotStart
        };
        const output = Mustache.render(sessionTemplate, data, partials);
        const fileName = path.join('public', dir, r.session.canonical);
        fs.writeFileSync(fileName, output, { encoding });      
      });
    }
  });
}

function renderSpeakerInstructions(partials) {
  const template = fs.readFileSync('speaker-instructions.mustache', encoding);
  const output = Mustache.render(template, {}, partials);
  fs.writeFileSync(path.join('public', 'speaker-instructions.html'), output, { encoding });
}

readPartials()
  .then(partials => {
    renderIndex(events, partials);
    events.forEach(e => renderEvent(e, partials));
    renderSpeakerInstructions(partials);
  });