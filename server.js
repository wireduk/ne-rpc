const express = require('express');
const app = express();
const port = 4000;

app.get('/*', express.static('public'));

app.listen(port, () => console.log('Server up'));